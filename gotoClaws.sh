#!/bin/bash
# remove the prefix from the url, call Claws-Mail
# and jump to the specific mail

prefix="claws://"
parameter=$1
mailURI=${parameter#$prefix}

claws-mail --select $mailURI

