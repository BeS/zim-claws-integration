#!/usr/bin/python
#
# mail2zim.py - convert a mail from Claws Mail into a list item on a zim wiki
#
# Copyright (C) 2016  Bjoern Schiessle <bjoern@schiessle.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, email, subprocess
from optparse import OptionParser

class MailHandler:

	def __init__(self, path):
		self.path = path

	def getSubject(self):

		subject = ''

		with open(self.path, 'r') as f:
			for line in f:
				if (line.startswith("Subject")):
					subject = line
					break

		return self.__decodeSubject(subject)


	def __decodeSubject(self, subject):

		default = 'ascii'
    
		headers=email.Header.decode_header(subject)

		for i, (text, charset) in enumerate(headers):
			try:
				headers[i]=unicode(text, charset or default, errors='replace')
			except LookupError:
				# if the charset is unknown, force default 
				headers[i]=unicode(text, default, errors='replace')

		return u" ".join(headers)


if __name__ == "__main__":

        parser = OptionParser("mail2todotxt.py path_to_mail")        
        (optionen, args) = parser.parse_args()
        
        mailUrl = args[0]
        
	mail = MailHandler(mailUrl)
	subject = mail.getSubject()
	task = subject.replace("Subject:", "")
        url = "url=claws://"+mailUrl
	subprocess.call(
                [
                        "zim",
                        "--plugin", "quicknote",
                        "--notebook", "Default",
                        "--page", ":Inbox",
                        "--text", task.strip(),
                        "--option", url.strip()
                ]
        )
