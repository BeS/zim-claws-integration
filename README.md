# Integration of the Zim desktop wiki in Claws Mail

This repository contains two files which allows you to integrate the Zim desktop
wiki in Claws Mail. Integration in this case means two things:

1. It enables you to create a Claws Mail action (mail2zim.py) which creates
   a new list item in a given Notebook and (sub-)page. Therefore the Zim
   Quick Note plugin is used. Mail2zim.py automatically adds the mail subject
   a reference to the mail, the date and allows you th add additional text.
   
2. A action (gotoClaws.sh) which can be configured in Zim for `claws://` links 
   which will remove the `claws://` prefix and then call Claws-Mail with the
   reference to the mail. This way Claws will jump directly to the mail.
